<?xml version="1.0" encoding="utf-8"?>
<!--
This file is part of groff, the GNU roff type-setting system.

Copyright (C) 2004, 2005, 2006, 2009, 2010,
2011, 2012, 2013 Free Software Foundation, Inc.
Written by Peter Schaffter (peter@schaffter.ca).

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3 or
any later version published by the Free Software Foundation; with the
Invariant Sections being this comment section, with no Front-Cover
Texts, and with no Back-Cover Texts.

A copy of the Free Documentation License is included as a file called
FDL in the main directory of the groff source package.
-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
  <title>Mom -- Graphics, floats, and preprocessor support</title>
  <link rel="stylesheet" type="text/css" href="stylesheet.css" />
</head>

<body style="background-color: #f5faff;">

<!-- ==================================================================== -->

<div id="top" class="page">

<!-- Navigation links -->
<table style="width: 100%;">
<tr>
  <td><a href="toc.html">Back to Table of Contents</a></td>
  <td style="text-align: right;"><a href="headfootpage.html#top">Next: Page headers/footers, pagination</a></td>
</tr>
</table>

<h1 class="docs">Graphics, floats, and preprocessor support</h1>

<div style="width: 55%; margin: auto;">
<ul class="no-enumerator" style="margin-left: -1em;">
  <li><a href="#images-intro">Introduction to inserting images and graphics</a>
  <li><a href="#converting">Image conversion and file processing</a>
  <ul style="margin-left: -.5em; list-style-type: disc;">
    <li><a href="#pdf">PDF</a></li>
    <li><a href="#eps">EPS</a></li>
  </ul></li>
  <li><a href="#pdf-image">The PDF_IMAGE macro</a></li>
  <li><a href="#pspic">The PSPIC macro</a></li>
	<li><a href="#preprocessor-support">Preprocessor support</a>
  <ul style="margin-left: -.5em; list-style-type: disc;">
  	<li><a href="#tbl">tbl</a>
		<ul style="margin-left: -1.25em;"><li><a href="#ts-te">.TS / .TH / .TE macros and arguments</a></li>
		</ul></li>
  	<li><a href="#pic">pic</a></li>
  	<li><a href="#eqn">eqn</a></li>
  	<li><a href="#refer">refer</a></li>
  </ul></li>
  <li><a href="#floats-intro">Introduction to floats</a></li>
  <li><a href="#float">The FLOAT macro</a></li>
</ul>
</div>

<div class="rule-medium"><hr/></div>

<h2 id="images-intro" class="docs">Introduction to inserting images and graphics</h2>

<p>
In order to include images in mom documents, the images must be in
either PDF (.pdf) or EPS (.eps) format.  Each format requires its own
macro, but both take the same arguments, and in the same order.
</p>

<p>
Please note that there are differences in the way the files
containing PDF and EPS images must be processed, hence documents may
not contain a mix.
</p>

<h3 id=converting class="docs">Image conversion and file processing</h3>

<p>
When your image files are not in PDF or EPS format&mdash;jpgs,
for example&mdash;you must convert them before including them in
a mom document.  Any utility for converting images may used.  The
ImageMagick suite of programmes, present on most GNU/Linux
systems, contains <b>convert</b>, which is simple and effective.
</p>

<h4 id="pdf" class="docs">PDF</h4>

<p>
Assuming a jpg image, conversion to PDF is done like this:
<br/>
<span class="pre-in-pp">
  convert &lt;image&gt;.jpg &lt;image&gt;.pdf
</span>
Any image type supported by <b>convert</b> may be converted this
way.
</p>

<p>
Mom files containing PDF images must be processed using
groff&#8217;s pdf driver.  Use of
<a href="using.html#pdfmom" style="font-weight: bold">pdfmom</a>
is strongly recommended, which natively invokes the pdf driver.
<br/>
<span class="pre-in-pp">
  pdfmom doc.mom &gt; doc.pdf
</span>
</p>

<h4 id="eps" class="docs">EPS</h4>

<p>
Assuming a jpg image, conversion to EPS is done like this:
<br/>
<span class="pre-in-pp">
  convert &lt;image&gt;.jpg &lt;image&gt;.eps
</span>
Any image type supported by <b>convert</b> may be converted this
way.  There have been reports of trouble with PostScript level 2
images, so don&#8217;t save your images in this format.
</p>

<p>
Mom files containing EPS images must be processed using
groff&#8217;s postscript driver.  Use of
<a href="using.html#pdfmom" style="font-weight: bold">pdfmom</a>,
which can be told to use the postscript driver, is strongly
recommended.
<br/>
<span class="pre-in-pp">
  pdfmom -Tps doc.mom &gt; doc.pdf
</span>
</p>

<!-- -PDF_IMAGE- -->

<div class="macro-id-overline">
<h3 id="pdf-image" class= "macro-id">PDF_IMAGE</h3>
</div>

<div class="box-macro-args">
Macro: <b>PDF_IMAGE</b> <kbd class="macro-args">[ -L | -C | -R | -I &lt;indent&gt; ] \
<br/>
&lt;pdf image&gt; &lt;width&gt; &lt;height&gt; \
<br/>
[ SCALE &lt;factor&gt; ] [ ADJUST +|-&lt;vertical adjustment&gt; ]</kbd>
</div>
<p class="requires">
&bull;&nbsp;<span style="font-style: normal">
<kbd>&lt;indent&gt;</kbd>,
<kbd>&lt;width&gt;</kbd>,
<kbd>&lt;height&gt;</kbd></span>
and
<span style="font-style: normal">
<kbd>&lt;vertical adjustment&gt;</kbd></span>
require a
<a href="definitions.html#unitofmeasure">unit of measure</a>
</p>

<p>
Unlike
<a href="#pspic">PSPIC</a>,
which it resembles, PDF_IMAGE requires that the pdf image&#8217;s
dimensions (the bounding box,
<a href="#bounding-box">see below</a>)
be supplied each time it&#8217;s called.
</p>

<p>
The first optional argument tells mom how to align the image
horizontally, with <kbd>-L</kbd>, <kbd>-C</kbd>, and <kbd>-R</kbd>
standing for left, centre and right respectively.  If you need more
precise placement, the <kbd>-I</kbd> argument allows you to give an
indent from the left margin.  Thus, to indent a PDF image 6
<a href="definitions.html#picaspoints">picas</a>
from the left margin
<br/>
<span class="pre-in-pp">
  .PDF_IMAGE -I 6P &lt;remaining arguments&gt;
</span>
If you omit the first argument, the image will be centred.
</p>

<p>
<kbd>&lt;pdf image&gt;</kbd> must be in PDF format, with a .pdf
extension.  If it is not, mom will abort with a message.  See <a
href="#pdf">here</a> for instructions on converting image formats to
PDF.
</p>

<p id="bounding-box">
<kbd>&lt;width&gt;</kbd> and <kbd>&lt;height&gt;</kbd> are the
dimensions of the image&#8217;s bounding box.  The most reliable way
of getting the bounding box is with the utility, <strong>pdfinfo</strong>:
<br/>
<span class="pre-in-pp">
  pdfinfo &lt;image.pdf&gt; | grep "Page *size"
</span>
This will spit out a line that looks like this:
<br/>
<span class="pre-in-pp">
  Page size:      width x height pts
</span>
<kbd>pts</kbd> means
<a href="definitions.html#picaspoints">points</a>,
therefore the unit of measure appended to <kbd>&lt;width&gt;</kbd>
and <kbd>&lt;height&gt;</kbd> must be <kbd>p</kbd>.
</p>

<p>
The optional <kbd>SCALE</kbd> argument allows you to scale the image
by <kbd>&lt;factor&gt;</kbd>.  The factor is a percentage of the
image&#8217;s original dimensions, thus
<br/>
<span class="pre-in-pp">
 SCALE 50
</span>
scales the image to 50 percent of its original size.  No percent
sign or unit of measure should be appended.
</p>

<p>
The final optional argument is the vertical adjustment to apply to
the image.  A plus value raises the image
<span style="font-style: italic">within the space allotted for it</span>;
a negative value lowers it.  The value must have a unit of measure
appended.
</p>

<p>
Remember that mom files with embedded PDF images must be processed
with
<br/>
<span class="pre-in-pp">
 pdfmom doc.mom &gt; doc.pdf
</span>
</p>

<div class="box-tip">
<p class="tip-top">
<span class="note">Note:</span>
Mom automatically applies shimming after PDF_IMAGE.  See
<a href="docprocessing.html#shim">SHIM</a>
for a discussion of shimming, and how to disable it.
<p>

<p>
<span class="note">Additional note:</span>
Mom treats single, discrete images inserted into a document with
PDF_IMAGE somewhat like
<a href="#floats-intro">floats</a>,
which is to say that if an image doesn&#8217;t fit on the output
page, she will defer it to the top of the next page while continuing
to process
<a href="definitions.html#running">running text</a>.
<kbd>ADJUST</kbd> is ignored whenever an image is deferred, and a
message is printed to stderr advising you where the deferment has
taken place.
</p>

<p class="tip-bottom">
However, if more than one image does not fit on the output page,
mom defers only the first; the remainder are silently ignored.
Therefore, if you insert several images close together in the text,
it is highly recommended that you wrap the images in floats, which
do not have this restriction.
</p>
</div>

<!-- -PSPIC- -->

<div class="macro-id-overline">
<h3 id="pspic" class= "macro-id">PSPIC</h3>
</div>

<div class="box-macro-args">
Macro: <b>PSPIC</b> <kbd class="macro-args">[ -L | -R | -I &lt;n&gt; ] &lt;file&gt; [ width [ height ] ]</kbd>
</div>

<p>
PSPIC is not actually part of mom, but rather a macro included with
every groff installation.  Although its arguments are identical to
PDF_IMAGE (except for <kbd>SCALE</kbd> and <kbd>ADJUST</kbd>, which
are missing), its behaviour is slightly different.
</p>

<p>
<kbd>man groff_tmac</kbd> contains the documentation for PSPIC, but
I&#8217;ll repeat it here with a few modifications for clarity.
</p>

<div class="examples-container">
<h3 id="groff-tmac" class="docs" style="margin-top: .5em;">From <span style="text-transform: none">groff_tmac</span></h3>
<p style="margin-top: .5em; margin-bottom: .5em;">
<kbd>&lt;file&gt;</kbd> is the name of the file containing the
image; <kbd>width</kbd> and <kbd>height</kbd> give the desired
width and height of the image as you wish it to appear within the
document.  The width and height arguments may have
<a href="definitions.html#unitofmeasure">units of measure</a>
attached; the default unit of measure is <kbd>i</kbd>.  PSPIC will
scale the graphic uniformly in the x and y directions so that
it is no more than <kbd>width</kbd> wide and <kbd>height</kbd>
high.  By default, the graphic will be horizontally centred.  The
<kbd>-L</kbd> and <kbd>-R</kbd> options cause the graphic to be
left-aligned and right-aligned, respectively.  The <kbd>-I</kbd>
option causes the graphic to be indented by <kbd>&lt;n&gt;</kbd>;
the default unit of measure is <kbd>m</kbd>
(<a href="definitions.html#em">ems</a>).
</p>
</div>

<p>
It is not necessary to pass PSPIC the <kbd>&lt;width&gt;</kbd>
and <kbd>&lt;height&gt;</kbd> arguments unless you are scaling
the image, in which case you will most likely need the original
dimensions of the EPS image&#8217;s bounding box.  These can be
found with
<span class="pre-in-pp">
 gs -q -dBATCH -dNOPAUSE -sDEVICE=bbox &lt;image file&gt;.pdf 2&gt;&amp;1 \
 | grep "%%BoundingBox" | cut -d " " -f4,5
</span>
The two digits returned are in
<a href="definitions.html#picaspoints">points</a>,
therefore the
<a href="definitions.html#unitofmeasure">unit of measure</a>
<kbd>p</kbd> must be appended to them.
</p>

<p>
Because PSPIC lacks the <kbd>ADJUST</kbd> option offered by
<a href="#pdf-image">PDF_IMAGE</a>
a certain amount of manual tweaking of the vertical placement of the
image will probably be required, typically by using the
<a href="typesetting.html#ald">ALD</a>
and
<a href="typesetting.html#rld">RLD</a>
macros.
</p>

<p>
Additionally, EPS images inserted into
<a href="definitions.html#running">running text</a>
will almost certainly disrupt the baseline placement of running
text.  In order to get mom back on track after invoking
<kbd>.PSPIC</kbd>, I strongly recommend using the
<a href="docprocessing.html#shim">SHIM</a>
macro so that the bottom margin of running text falls where it
should.  Please note that with PDF_IMAGE, this is not necessary.
</p>

<p>
Remember that mom files with embedded EPS images must be processed
with
<br/>
<span class="pre-in-pp">
 pdfmom -Tps doc.mom &gt; doc.pdf
</span>
</p>

<div class="rule-medium"><hr/></div>

<h2 id="preprocessor-support" class="docs">Preprocessor support</h2>

<h3 id="tbl" class="docs">tbl support</h3>

<p>
Mom documents can include tables generated with the groff
pre-processor, <kbd>tbl</kbd>. <kbd>tbl</kbd> usage itself is beyond
the scope of this documentation, but is covered in the manpage
<kbd>tbl(1)</kbd>.  You can also download a copy of
<a href="http://plan9.bell-labs.com/10thEdMan/tbl.pdf">Tbl - A Program to Format Tables</a>,
which, in addition to providing a thorough introduction to <kbd>tbl</kbd>,
contains some fine examples.
</p>

<p>
Tables formatted with <kbd>tbl</kbd> begin with the macro
<kbd>.TS</kbd> (<b>T</b>able <b>S</b>art) and end with
<kbd>.TE</kbd> (<b>T</b>able <b>E</b>nd).  Depending on where you
want your tables output in a document, you may need to wrap
your <kbd>tbl</kbd> code inside a
<a href="#floats-intro">float</a>,
or pass the <kbd>H</kbd> argument to <kbd>.TS</kbd>.
</p>

<p>
If you put <kbd>tbl</kbd> code inside a float, the table will be
output immediately if it fits on the page, or deferred to the top
of the next page if it doesn&#8217;t.  If you prefer a table to
begin where you say and span over to the next page, or if you know
for certain a boxed table will run to multiple pages, simply pass the
<kbd>H</kbd> argument to <kbd>.TS</kbd>, along with a corresponding
<a href="#th"><kbd>TH</kbd></a>.
</p>

<div class="box-tip">
<p class="tip">
<span class="note">Note:</span>
If you use <kbd>.TS H</kbd> to create a boxed table that spans
multiple pages, do not attempt to wrap the table inside a float.
For the purposes of boxed, multipage tables, <kbd>.TS H</kbd> and
<kbd>.FLOAT</kbd> should be considered mutually exclusive.  This
restriction is imposed by the <kbd>tbl</kbd> preprocessor itself,
not groff or mom.
</p>
</div>

<h4 id="tbl-placement" class="docs">tbl placement in mom docs</h4>

<p>
If you use <kbd>.TS</kbd> without the <kbd>H</kbd> argument (and
therefore no <kbd>.TH</kbd>), tables that fit on the page are output
in position.  If there is not enough room to output the table,
<kbd>tbl</kbd> will abort with message instructing you to use
<kbd>.TS H/.TH</kbd>.
</p>

<p>
If you give <kbd>.TS</kbd> the <kbd>H</kbd> argument (with a
corresponding <kbd>.TH</kbd>), tables will be output in position and
span as many pages as necessary to complete output.  A table header
will be printed at the top of each page&#8217;s table output.  In the
event that there is not enough room to print the table header and
at least one row of table data near the bottom of a page, mom will
break to a new page before beginning table output, leaving a blank
in
<a href="definitions.html#running">running text</a>.
</p>

<p>
Boxed tables inside
<a href="#floats-intro">floats</a>
are output in position if they fit on the page.  If not, they are
deferred to the top of the next page without a break in running
text.  Boxed tables within floats may not, however, span multiple
pages; mom will abort with a message should a boxed table in a float
run longer than the page.
</p>

<p>
Unboxed tables inside floats may span multiple pages provided the
<kbd>SPAN</kbd> argument has been given to
<a href="#float">FLOAT</a>.
</p>

<div class="box-tip">
<p class="tip">
<span class="note">Note:</span>
The vertical spacing around unfloated tables may appear slightly
unequal, especially if there are several tables on the page.  This
is a result of
<a href="docprocessing.html#shim">shimming</a>
that mom applies automatically after each table.  You may
disable shimming with <kbd>.NO_SHIM</kbd>, or by giving the
<kbd>NO_SHIM</kbd> argument to <kbd>.TE</kbd>.  In either case, you
will still likely want to adjust the spacing around with table with
<a href="typesetting.html#ald">ALD</a>
or
<a href="typesetting.html#rld">RLD</a>.
Tables inside floats are more easily adjusted with the
<kbd>ADJUST</kbd> argument to
<a href="#float">FLOAT</a>.
</p>
</div>

<div class="macro-id-overline">
<h3 id="ts-te" class= "macro-id">.TS / .TH / .TE</h3>
</div>

<div class="box-macro-args">
Macro: <a href="#ts"><b>TS</b></a> <kbd class="macro-args">[ H ]  [ BOXED ]  [ CENTER ]  [ NEEDS ]</kbd>
<br/>
Macro: <a href="#th"><b>TH</b></a> <kbd class="macro-args">(optional, only if .TS H)</kbd>
<br/>
Macro: <a href="#te"><b>TE</b></a> <kbd class="macro-args">[ "caption" ] [ &lt;distance&gt; ] [ LEFT | CENTER | RIGHT ] [ NO_SHIM ]</kbd>
</div>

<p>
Tables to be formatted with <kbd>tbl</kbd> begin with the macro
<kbd>.TS</kbd> and end with <kbd>.TE</kbd>.  Global <kbd>tbl</kbd>
options (&#8220;flags&#8221;), formatting, and data (per
<kbd>tbl(1)</kbd>) come between the two macros.
<br/>
<span class="pre-in-pp">
  .TS
  &lt;tbl options, formatting, and data&gt;
  .TE
</span>
Tables may be wrapped inside a
<a href="#float-intro">float</a>,
in which case, the entire table will be output on the current page
if it fits, or deferred to the next page if it doesn&#8217;t.
<br/>
<span class="pre-in-pp">
  .FLOAT
  .TS
  &lt;tbl options, formatting, and data&gt;
  .TE
  .FLOAT OFF
</span>
</p>

<div class="macro-id-overline">
<h4 id="ts" class="docs" style="font-size: 100%; margin-top: .5em">The .TS macro</h4>
</div>

<p>
The <b>TS</b> macro must be invoked before entering a <kbd>tbl</kbd>
block.  You may give as many or as few of its arguments as required,
in any order.
</p>

<h5 class="docs" style="margin-top: 1em; text-transform: none">'H'</h5>

<p>
With the <b>H</b> argument, a table will span as many pages as
necessary, with or without a running header.  The placement of the
corresponding
<a href="#th"><kbd>.TH</kbd></a>,
which is required whenever the <b>H</b> argument is given,
determines what, if anything, goes in the header.  Compare the
following:
<span class="pre-in-pp">
  .TS H                   .TS H
  c s s                   c s s
  c s s                   c s s
  c c c                   c c c
  n n n.                  n n n. 
  Percent Increase        .TH
  2002-2012               Percent Increase
  .TH                     2002-2012
  &lt;tbl data&gt;              &lt;tbl data&gt;
  .TE                     .TE
</span>
The first example will create a table that spans multiple
pages if necessary, with a running header (&#8220;Percent
Increase&nbsp;/&nbsp;2002-2012&#8221;) for that table appearing at
the top of each page until the table ends.  The second example,
equally, may run to several pages, but without the running header.
See
<a href="#th"><b>TH</b></a>
for an explanation of <kbd>.TH</kbd> placement.
</p>

<div id="h-tip" class="box-tip">
<p class="tip">
<span class="note">Tip:</span>
Generally speaking, it&#8217;s a good idea to get into the habit
of using <kbd>.TS H</kbd> all the time, since there are no
circumstances under which it fails, whereas <kbd>.TS</kbd> without
<kbd>H</kbd> will fail on tables that exceed the page length.
</p>
</div>

<h5 class="docs" style="margin-top: 1em; text-transform: none">'BOXED'</h5>

<p>
If a table is to be boxed (ie <kbd>tbl</kbd> is given the flags
<kbd>'box'</kbd> or <kbd>'allbox'</kbd>) you must pass the argument
<kbd>BOXED</kbd> to <kbd>.TS</kbd>, as in this example:
<br/>
<span class="pre-in-pp">
  .TS BOXED
  allbox;
  c s s
  c c c
  n n n.
  &lt;tbl data&gt;
  .TE
</span>
</p>

<h5 class="docs" style="margin-top: 1em; text-transform: none">'CENTER'</h5>

<p>
If a table is to be centered on the page, (ie <kbd>tbl</kbd> is
given the <kbd>'center'</kbd> flag), you must pass the argument
<kbd>CENTER</kbd> to <kbd>.TS</kbd>, as in this example, which
creates a (possibly) multipage boxed table, centered on the page,
with a running header.
<span class="pre-in-pp">
  .TS H BOXED CENTER
  allbox center;
  c s s
  c s s
  c c c
  n n n.
  Percent Increase
  2002-2012
  .TH
  &lt;tbl data&gt;
  .TE
</span>
</p>

<h5 class="docs" style="margin-top: 1em; text-transform: none">'NEEDS'</h5>

<p>
If a table is not inside a float and you pass <kbd>.TS </kbd> the
<kbd>H</kbd> argument (which you should; see the tip
<a href="#h-tip">here</a>),
mom begins output immediately where the table occurs in the input
file <i>if there is enough room on the output page for the table
header plus at least one row of table data</i>.  If there isn't
enough room, mom breaks to a new page before beginning the table,
leaving a gap in
<a href="definitions.html#running">running text</a>
at the bottom of the previous page.  If, for aesthetic reasons,
you would prefer that mom require more than one row of table data
beneath the header near the bottom of a page, you may increase the
number with the <kbd>NEEDS</kbd> argument, followed by the desired
number of rows.
</p>

<div class="macro-id-overline">
<h4 id="ts" class="docs" style="font-size: 100%; margin-top: .5em">The .TH macro</h4>
</div>

<p>
The <b>TH</b> macro (<b>T</b>able <b>H</b>eader), which is required
when you begin a table with <kbd>.TS H</kbd>, allows you to
determine what goes in a table&#8217;s running header if it spans
multiple pages.  If you place it immediately underneath your
<kbd>tbl</kbd> formatting specifications, the last line of which
always ends with a period (see <kbd>tbl(1)</kbd>), there will
be no running header.  If you place it under the first row of
<kbd>tbl</kbd> data, the first row will form the header; under the
second row, the first and second rows form the header, and so on.
</p>

<div class="macro-id-overline">
<h4 id="ts" class="docs" style="font-size: 100%; margin-top: .5em">The .TE macro</h4>
</div>

<p>
<kbd>tbl</kbd> blocks must be terminated with <kbd>.TE</kbd>.
Arguments to <b>TE</b> are optional.  If <kbd>&#8220;&lt;caption&gt;&#8221;</kbd>
is given, you may use as many or as few of the subsequent arguments
as you wish, in any order.
</p>

<h5 class="docs" style="margin-top: 1em; text-transform: none">&#8220;&lt;caption&gt;&#8221;</h5>

<p>
If you wish a table to have a caption, or label, underneath, surround the
caption in double-quotes after <kbd>.TE</kbd>, like this:
<span class="pre-in-pp">
  .TE "Table 1"
</span>
</p>

<h5 class="docs" style="margin-top: 1em; text-transform: none">&lt;distance&gt;</h5>

<p>
If you would like to increase the space between the bottom of a
table and its caption, enter the increase with a digit to which is
appended a
<a href="definitions.html#unitofmeasure">unit of measure</a>:
<span class="pre-in-pp">
  .TE "Table 1" 3p
</span>
</p>

<h5 class="docs" style="margin-top: 1em; text-transform: none">LEFT | CENTER | RIGHT</h5>

<p>
By default, mom aligns captions with the left side of tables.  If
this is what you want, you may enter <kbd>LEFT</kbd>, or simply skip
stating where you want the caption.  If you would prefer centered or
right placement, use <kbd>CENTER</kbd> or <kbd>RIGHT</kbd>.
</p>

<h5 class="docs" style="margin-top: 1em; text-transform: none">NO_SHIM</h5>

<p>
By default, mom adds
<a href="docprocessing.html#shim">shimming</a>
underneath tables.  This behaviour may be disabled with the
<kbd>NO_SHIM</kbd> argument, which, moreover, can be used for tables
without a caption, like this:
<span class="pre-in-pp">
  .TE NO_SHIM
</span>
</p>

<div class="rule-medium"><hr/></div>

<h3 id="pic" class="docs">pic support</h3>

<p>
At present, mom has no integrated support or special features
for the <b>pic</b> preprocessor, however <b>pic</b> may be used
successfully within a mom document.  Generally, it&#8217;s best to
wrap <b>pic</b> blocks within a float (see below).  If you want
your <b>pic</b>s centred, you must include <kbd>-mpic</kbd> in the
options passed to <kbd>pdfmom</kbd> or <kbd>groff</kbd>.
</p>

<h3 id="eqn" class="docs">eqn support</h3>

<p>
At present, mom has no integrated support or special features
for the <b>eqn</b> preprocessor, however <b>eqn</b> may be used
successfully within a mom document.  Generally, it&#8217;s best to
wrap <b>eqn</b> blocks within a float (see below).
</p>

<h3 id="refer" class="docs">refer support</h3>

<p>
<b>refer</b> support is covered in the section
<a href="refer.html">Bibliographies and references</a>.
</p>

<div class="rule-medium"><hr/></div>

<h2 id="floats-intro" class="docs">Introduction to floats</h2>

<p>
Images and graphics (including those created with
<strong>tbl</strong> and <strong>pic</strong>) sometimes do not
fit on the output page of a PDF or PostScript document at the
place they&#8217;re inserted in the input file.  It&#8217;s
necessary, therefore, to defer them to the next page while carrying
on with
<a href="definitions.html#running">running text</a>.
</p>

<p>
Whenever you need this functionality (tables, for example, generally
need only appear near related text, not at a precise location), mom
provides the FLOAT macro.
</p>

<p>
Floats are usually used for images and graphics, but can contain
anything you like, including text.  Whatever&#8217;s in the
float will be kept together as a block, output immediately if
there&#8217;s room, or deferred to the top of the next output page
when there isn&#8217;t; running text continues to the bottom of the
previous page without interruption.
</p>

<p>
In the case of a float that doesn&#8217;t fit being followed by
one that does, the second is output in position and the first is
deferred.  In the case of two or more that don&#8217;t fit, they are
output in order on the next page.
</p>

<p>
A key distinction between a float and a
<a href="docelement.html#quote">QUOTE</a>
or
<a href="docelement.html#blockquote">BLOCKQUOTE</a>
is that while a float keeps everything together and defers output if
necessary, quotes and blockquotes are output immediately, and may
start on one page and finish on the next.
</p>

<p>
Floats always deposit a break before they begin, which means the
line beforehand will not be
<a href="definitions.html#filled">filled</a>.
Floats, therefore, cannot be inserted in the middle of a paragraph
without studying the output file and determining where to break or
<a href="typesetting.html#spread">spread</a>
the line before the float.
</p>

<p id="float-spacing">
Floats begin on the baseline immediately below the running text
preceding them.  No additional whitespace surrounds them, above or
below.  Running text below a float is, however,
<a href="docprocessing.html#shim">shimmed</a>,
unless shimming has been disabled with <kbd>.NO_SHIM</kbd>.  This
usually results in a small amount of extra whitespace after the
float.  The <kbd>ADJUST</kbd> argument to FLOAT allows you to
fine-tune the vertical centering.
</p>

<p>
If you&#8217;d like more space around a float, you must add it
manually, for example with 
<a href="typesetting.html#ald">ALD</a>
or
<a href="typesetting.html#space">SPACE</a>.
</p>

<!-- -FLOAT- -->

<div class="macro-id-overline">
<h3 id="float" class= "macro-id">FLOAT</h3>
</div>

<div class="box-macro-args">
Macro: <b>FLOAT</b> <kbd class="macro-args">[ FORCE ] [ ADJUST +|-&lt;amount&gt; ] [ SPAN ] | &lt;anything&gt;</kbd>
</div>

<div class="box-tip">
<p class="tip">
<span class="note">Note:</span>
FLOAT is intended for use with the document processing macros only.
</p>
</div>

<p style="margin-top: -.5em">
To begin a float, simply invoke <kbd>.FLOAT</kbd> and follow it with
whatever you want the float to contain.  When you&#8217;re done,
invoke <kbd>.FLOAT&nbsp;OFF</kbd> (or <kbd>QUIT, END, X</kbd>, etc).
</p>

<p>
The optional <kbd>FORCE</kbd> argument instructs mom to output
the float exactly where it occurs in the input file.  With
<kbd>FORCE</kbd>, mom immediately breaks to a new page to output
the float if it does not fit on the current page.  While this is
somewhat contrary to the notion of floats (ie that running text
should continue to fill the page), there are circumstances where it
may be desirable.
</p>

<p>
The <kbd>ADJUST</kbd> argument tells mom to raise
(<kbd>+</kbd>) or lower (<kbd>-</kbd>) the float <i>within the space
allotted to it</i> by the specified amount.
<kbd>&lt;amount&gt;</kbd> must have a
<a href="definitions.html#unitofmeasure">unit of measure</a>
appended.  <kbd>ADJUST</kbd> gives you precise control over
the vertical centering of floats, allowing you to compensate for
unequal spacing that may result of from the automatic shimming of
floats (or the absence thereof).  See
<a href="docprocessing.html#shim">SHIM</a>
for a discussion of automatic shimming.
</p>

<p>
<kbd>ADJUST</kbd> is ignored whenever a float is deferred to
the following page.
</p>

<p>
The <kbd>SPAN</kbd> argument tells mom that a float, if deferred,
may carry onto multiple pages.  Please note that <kbd>SPAN</kbd> may
not be used for floats containing a boxed table; mom will abort with
a warning should this occur.  Unboxed tables, on the other hand, are
acceptable within floats that are given the <kbd>SPAN</kbd> argument.
</p>

<div class="box-tip">
<p class="tip-top">
<span class="note">Note:</span>
Floats use
<a href="definitions.html#filled">no-fill mode</a>,
with each input line beginning at the left margin.  If this is not
what you want, you must specify the preferred horizontal alignment
<i>within the float</i> (eg
<a href="typesetting.html#lrc">CENTER</a>
or
<a href="typesetting.html#lrc">RIGHT</a>).
</p>

<p class="tip-bottom">
Furthermore, if you want text
<a href="definitions.html#filled">filled</a>,
you must specify
<a href="typesetting.html#quad"><kbd>.QUAD&nbsp;L|R|C</kbd></a>
or
<a href="typesetting.html#justify"><kbd>.JUSTIFY</kbd></a>&mdash;again,
within the float.
</p>
</div>

<div class="rule-long"><hr/></div>

<!-- Navigation links -->
<table style="width: 100%; margin-top: 12px;">
<tr>
  <td style="width: 33%;"><a href="toc.html">Back to Table of Contents</a></td>
  <td style="width: 20%; text-align: center;"><a href="#top">Top</a></td>
  <td style="width: 46%; text-align: right;"><a href="headfootpage.html">Next: Page headers/footers, pagination</a></td>
</tr>
</table>

</div>

<div class="bottom-spacer"><br/></div>

</body>
</html>
<!-- vim: fileencoding=utf-8: nomodified: -->
